//!HOOK SCALED
//!BIND HOOKED
//!DESC Contrast Adaptive Sharpening [0.40]

#define SHARPNESS 0.40  // Sharpening strength

#define saturate(x) clamp(x, 0.0, 1.0)

const float peak = -1.0 / mix(8.0, 5.0, saturate(SHARPNESS));

vec4 hook() {
	// fetch a 3x3 neighborhood around the pixel 'e',
	//	a b c
	//	d(e)f
	//	g h i
	vec3 a = HOOKED_texOff(ivec2(-1, -1)).rgb;
	vec3 b = HOOKED_texOff(ivec2( 0, -1)).rgb;
	vec3 c = HOOKED_texOff(ivec2( 1, -1)).rgb;
	vec3 d = HOOKED_texOff(ivec2(-1,  0)).rgb;
	vec3 e = HOOKED_texOff(ivec2( 0,  0)).rgb;
	vec3 f = HOOKED_texOff(ivec2( 1,  0)).rgb;
	vec3 g = HOOKED_texOff(ivec2(-1,  1)).rgb;
	vec3 h = HOOKED_texOff(ivec2( 0,  1)).rgb;
	vec3 i = HOOKED_texOff(ivec2( 1,  1)).rgb;

	// Soft min and max.
	//	a b c			  b
	//	d e f * 0.5	 +	d e f * 0.5
	//	g h i			  h
	// These are 2.0x bigger (factored out the extra multiply).
    vec3 mnRGB = min(min(min(d, e), min(f, b)), h);
    vec3 mnRGB2 = min(mnRGB, min(min(a, c), min(g, i)));
    mnRGB += mnRGB2;

    vec3 mxRGB = max(max(max(d, e), max(f, b)), h);
    vec3 mxRGB2 = max(mxRGB, max(max(a, c), max(g, i)));
    mxRGB += mxRGB2;

	// Smooth minimum distance to signal limit divided by smooth max.
	vec3 ampRGB = saturate(min(mnRGB, 2.0 - mxRGB) / mxRGB);
	
	// Shaping amount of sharpening.
	vec3 wRGB = sqrt(ampRGB) * peak;
	
	// Filter shape.
	//  0 w 0
	//  w 1 w
	//  0 w 0  
	vec3 weightRGB = 1.0 + 4.0 * wRGB;
	vec3 window = (b + d) + (f + h);
	return vec4(saturate((window * wRGB + e) / weightRGB).rgb, HOOKED_tex(HOOKED_pos).a);
}