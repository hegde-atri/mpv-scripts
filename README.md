# mpv-scripts
my mpv scripts so i don't loose them

place them in appdata/roaming/mpv/shaders

and then ```glsl-shader="~~/shaders/[shader file]"``` in mpv.conf


# Disclaimer
None of these scripts are made by me and belong to the respective owners. If you want me to take it down, please email me at dev.hegdeatri@gmail.com
